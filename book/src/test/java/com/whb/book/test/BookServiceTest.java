/*
 * Project: book
 * 
 * File Created at 2017年9月11日
 * 
 * Copyright 2016 CMCC Corporation Limited.
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * ZYHY Company. ("Confidential Information").  You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license.
 */
package com.whb.book.test;

import javax.annotation.Resource;

import org.junit.Assert;
import org.junit.Test;

import com.whb.book.entity.Book;
import com.whb.book.service.BookService;

/**
 * @Type BookServiceTest.java
 * @Desc 
 * @author wanghb
 * @date 2017年9月11日 下午8:00:40
 * @version 
 */
public class BookServiceTest extends BaseTest{

    @Resource
    private BookService bookService;
    
    @Test
    public void testQueryById(){
        Long id = 10000L;
        Book book = bookService.findBookById(id);
        Assert.assertEquals("金瓶梅", book.getBookName());
    }
}


/**
 * Revision history
 * -------------------------------------------------------------------------
 * 
 * Date Author Note
 * -------------------------------------------------------------------------
 * 2017年9月11日 wanghb create
 */