/*
 * Project: book_manage
 * 
 * File Created at 2017年6月11日
 * 
 * Copyright 2016 CMCC Corporation Limited.
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * ZYHY Company. ("Confidential Information").  You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license.
 */
package com.whb.book.test;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import com.whb.book.dao.BookDao;
import com.whb.book.entity.Book;

/**
 * @Type BookDaoTest.java
 * @Desc 
 * @author wanghb
 * @date 2017年6月11日 下午3:58:27
 * @version 
 */
public class BookDaoTest extends BaseTest{
    
    @Autowired
    private BookDao bookDao;
    
    @Test
    public void testQueryById() throws Exception {
        Long id = 10000L;
        Book book = bookDao.query(id);
        System.out.println(book);
    }

}


/**
 * Revision history
 * -------------------------------------------------------------------------
 * 
 * Date Author Note
 * -------------------------------------------------------------------------
 * 2017年6月11日 wanghb create
 */