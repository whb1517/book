CREATE TABLE user_info (
 	ID 				int UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '主键',
 	staff_id 		varchar(20) DEFAULT NULL COMMENT '学生学号',
  	username 		varchar(10) DEFAULT NULL COMMENT '用户姓名',
  	gender 			char(1) DEFAULT NULL COMMENT '性别0|女，1|男',
  	birth 			datetime DEFAULT NULL COMMENT '出生日期',
  	msisdn 			varchar(20)  DEFAULT NULL COMMENT '手机号',
  	email 			varchar(40) DEFAULT NULL COMMENT '邮箱',
    hometown        varchar(40) DEFAULT NULL COMMENT '家乡',
  	entry_time 		datetime DEFAULT NULL COMMENT '入学时间',
    college         varchar(40) DEFAULT NULL COMMENT '学院',
    major           varchar(10) DEFAULT NULL COMMENT '专业',
  	grade           tinyint DEFAULT NULL COMMENT '班级，1|一班，2|二班',
  	create_id 		int UNSIGNED DEFAULT NULL COMMENT '创建者',
  	update_id 		int UNSIGNED DEFAULT NULL COMMENT '更新者',
  	create_time     datetime DEFAULT NULL COMMENT '创建时间',
  	update_time     datetime DEFAULT NULL COMMENT '更新时间',
  	PRIMARY KEY (id)
) 	ENGINE=InnoDB AUTO_INCREMENT=10000 DEFAULT CHARSET=utf8 COMMENT='用户表';


CREATE TABLE book (
 	ID 				int UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '主键',
 	book_name 		varchar(20) DEFAULT NULL COMMENT '书名',
  	author  		varchar(10) DEFAULT NULL COMMENT '作者',
  	year 			varchar(10) DEFAULT NULL COMMENT '年份',
  	type 			tinyint DEFAULT NULL COMMENT '类型',
  	create_id 		int UNSIGNED DEFAULT NULL COMMENT '创建者',
  	update_id 		int UNSIGNED DEFAULT NULL COMMENT '更新者',
  	create_time     datetime DEFAULT NULL COMMENT '创建时间',
  	update_time     datetime DEFAULT NULL COMMENT '更新时间',
  	PRIMARY KEY (id)
) 	ENGINE=InnoDB AUTO_INCREMENT=10000 DEFAULT CHARSET=utf8 COMMENT='图书表';

CREATE TABLE user_book_relation (
 	ID 				int UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '主键',
 	user_id 		varchar(20) DEFAULT NULL COMMENT '书名',
  	book_id  		varchar(10) DEFAULT NULL COMMENT '作者',
    borrow_status   tinyint DEFAULT NULL COMMENT '借阅状态，0|借，1|已归还，2|过期未归还',
  	create_id 		int UNSIGNED DEFAULT NULL COMMENT '创建者',
  	update_id 		int UNSIGNED DEFAULT NULL COMMENT '更新者',
  	create_time     datetime DEFAULT NULL COMMENT '创建时间',
  	update_time     datetime DEFAULT NULL COMMENT '更新时间',
  	PRIMARY KEY (id)
) 	ENGINE=InnoDB AUTO_INCREMENT=10000 DEFAULT CHARSET=utf8 COMMENT='学生图书关系表';
