/*
 * Project: book
 * 
 * File Created at 2017年9月14日
 * 
 * Copyright 2016 CMCC Corporation Limited.
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * ZYHY Company. ("Confidential Information").  You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license.
 */
package com.whb.book.dto;

import java.util.Date;

/**
 * @Type SyncUserVO.java
 * @Desc 
 * @author wanghb
 * @date 2017年9月14日 下午9:45:51
 * @version 
 */
public class SyncUserVO{

    private String staffId;
    private String username;
    private String gender;
    private Date birth;
    private String msisdn;
    private String email;
    private String hometown;
    private Date entryTime;
    private String college;
    private String major;
    private String grade;
    private int borrowStatus;
    public String getStaffId() {
        return staffId;
    }
    public void setStaffId(String staffId) {
        this.staffId = staffId;
    }
    public String getUsername() {
        return username;
    }
    public void setUsername(String username) {
        this.username = username;
    }
    public String getGender() {
        return gender;
    }
    public void setGender(String gender) {
        this.gender = gender;
    }
    public Date getBirth() {
        return birth;
    }
    public void setBirth(Date birth) {
        this.birth = birth;
    }
    public String getMsisdn() {
        return msisdn;
    }
    public void setMsisdn(String msisdn) {
        this.msisdn = msisdn;
    }
    public String getEmail() {
        return email;
    }
    public void setEmail(String email) {
        this.email = email;
    }
    public String getHometown() {
        return hometown;
    }
    public void setHometown(String hometown) {
        this.hometown = hometown;
    }
    public Date getEntryTime() {
        return entryTime;
    }
    public void setEntryTime(Date entryTime) {
        this.entryTime = entryTime;
    }
    public String getCollege() {
        return college;
    }
    public void setCollege(String college) {
        this.college = college;
    }
    public String getMajor() {
        return major;
    }
    public void setMajor(String major) {
        this.major = major;
    }
    public String getGrade() {
        return grade;
    }
    public void setGrade(String grade) {
        this.grade = grade;
    }
    public int getBorrowStatus() {
        return borrowStatus;
    }
    public void setBorrowStatus(int borrowStatus) {
        this.borrowStatus = borrowStatus;
    }
    @Override
    public String toString() {
        return "SyncUserVO [staffId=" + staffId + ", username=" + username + ", gender=" + gender
                + ", birth=" + birth + ", msisdn=" + msisdn + ", email=" + email + ", hometown="
                + hometown + ", entryTime=" + entryTime + ", college=" + college + ", major="
                + major + ", grade=" + grade + ", borrowStatus=" + borrowStatus + "]";
    }
}


/**
 * Revision history
 * -------------------------------------------------------------------------
 * 
 * Date Author Note
 * -------------------------------------------------------------------------
 * 2017年9月14日 wanghb create
 */