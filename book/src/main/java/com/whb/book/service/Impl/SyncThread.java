/*
 * Project: book
 * 
 * File Created at 2017年9月14日
 * 
 * Copyright 2016 CMCC Corporation Limited.
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * ZYHY Company. ("Confidential Information").  You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license.
 */
package com.whb.book.service.Impl;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import com.whb.book.common.exception.BookException;
import com.whb.book.common.exception.BusinessError.ErrorCode;
import com.whb.book.common.util.DateUtil;
import com.whb.book.dao.BookDao;
import com.whb.book.dao.UserBookRelationDao;
import com.whb.book.dao.UserDao;
import com.whb.book.dto.SyncUserVO;
import com.whb.book.entity.Book;
import com.whb.book.entity.UserBookRelation;
import com.whb.book.entity.UserInfo;

/**
 * @Type SyncThread.java
 * @Desc 
 * @author wanghb
 * @date 2017年9月14日 下午10:26:21
 * @version 
 */
public class SyncThread implements Runnable {

    private static Logger logger = LoggerFactory.getLogger(SyncThread.class);

    private List<SyncUserVO> list;
    private Long bookId;

    public SyncThread(List<SyncUserVO> list, Long bookId) {
        super();
        this.list = list;
        this.bookId = bookId;
    }

    @Autowired
    private BookDao bookDao;

    @Autowired
    private UserDao userDao;

    @Autowired
    private UserBookRelationDao ubrDao;

    @Override
    public void run() {
        for (SyncUserVO suvo : list) {
            Book book = bookDao.query(bookId);
            if (book == null) {
                logger.error("根据booId={}查询book表为空,停止同步", bookId);
                throw new BookException(ErrorCode.APP_NOT_FOUND, "书籍不存在");
            }
            UserInfo user = userDao.queryByMsisdn(suvo.getMsisdn());
            if (user == null) {
                logger.info("库里没有该学生，故新增至user_info表");
                addUser(suvo);
            } else {
                logger.info("库里以已经存在该学生，故更新user_info表中数据");
                updateUser(suvo);
            }
            UserBookRelation ubr = ubrDao.query(user.getId(), bookId);
            if (ubr == null) {
                ubrDao.save(ubr);
            } else {
                ubrDao.update(ubr);
            }
        }
    }

    private void addUser(SyncUserVO suvo) {
        UserInfo user = new UserInfo();
        user.setBirth(suvo.getBirth());
        user.setCollege(suvo.getCollege());
        user.setCreateId(0L);
        user.setCreateTime(DateUtil.getCurrentDate());
        user.setEmail(suvo.getEmail());
        user.setEntryTime(suvo.getEntryTime());
        user.setGender(Integer.parseInt(suvo.getGender()));
        user.setGrade(Integer.parseInt(suvo.getGrade()));
        user.setHometown(suvo.getHometown());
        user.setUsername(suvo.getUsername());
        user.setUpdateTime(DateUtil.getCurrentDate());
        user.setUpdateId(0L);
        user.setMsisdn(suvo.getMsisdn());
        user.setMajor(suvo.getMajor());
        userDao.save(user);
        logger.info("新增user_info表成功");
    }

    private void updateUser(SyncUserVO suvo) {
        UserInfo user = new UserInfo();
        user.setBirth(suvo.getBirth());
        user.setCollege(suvo.getCollege());
        user.setCreateId(0L);
        user.setCreateTime(DateUtil.getCurrentDate());
        user.setEmail(suvo.getEmail());
        user.setEntryTime(suvo.getEntryTime());
        user.setGender(Integer.parseInt(suvo.getGender()));
        user.setGrade(Integer.parseInt(suvo.getGrade()));
        user.setHometown(suvo.getHometown());
        user.setUsername(suvo.getUsername());
        user.setUpdateTime(DateUtil.getCurrentDate());
        user.setUpdateId(0L);
        user.setMsisdn(suvo.getMsisdn());
        user.setMajor(suvo.getMajor());
        userDao.update(user);
        logger.info("更新user_info表成功");
    }

}

/**
 * Revision history
 * -------------------------------------------------------------------------
 * 
 * Date Author Note
 * -------------------------------------------------------------------------
 * 2017年9月14日 wanghb create
 */
