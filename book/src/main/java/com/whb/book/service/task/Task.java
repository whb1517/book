/*
 * Project: book
 * 
 * File Created at 2017年9月10日
 * 
 * Copyright 2016 CMCC Corporation Limited.
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * ZYHY Company. ("Confidential Information").  You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license.
 */
package com.whb.book.service.task;

import java.util.Date;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

/**
 * @Type Task.java
 * @Desc 
 * @author wanghb
 * @date 2017年9月10日 上午10:57:43
 * @version 
 */
@Component
public class Task {

    private static Logger logger = LoggerFactory.getLogger(Task.class);

    @Scheduled(cron = "0 20 20 * * *")
    public void task1() {
        logger.debug("现在是凌晨1点{},开始跑定时任务啦。。。", new Date());
        System.out.println("跑定时任务啦");
    }
    
    @Scheduled(cron = "0 0/5 14,18 * * ?")
    public void task2() {
        logger.debug("在每天下午2点到2:55期间和下午6点到6:55期间的每5分钟触发 {}", new Date());
        System.out.println("跑定时任务啦");
    }
}

/**
 * Revision history
 * -------------------------------------------------------------------------
 * 
 * Date Author Note
 * -------------------------------------------------------------------------
 * 2017年9月10日 wanghb create
 */
