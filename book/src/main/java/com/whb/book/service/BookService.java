/*
 * Project: book_manage
 * 
 * File Created at 2017年9月6日
 * 
 * Copyright 2016 CMCC Corporation Limited.
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * ZYHY Company. ("Confidential Information").  You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license.
 */
package com.whb.book.service;

import java.util.List;

import com.whb.book.dto.SyncUserVO;
import com.whb.book.entity.Book;

/**
 * @Type bookService.java
 * @Desc 
 * @author wanghb
 * @date 2017年9月6日 下午9:45:55
 * @version 
 */
public interface BookService {
    
    /** 
     * findBookList:(). <br/>
     * 
     * @author wanghb
     * @return
     * @since JDK 1.8
     */
    public List<Book> findBookList();
    
    /** 
     * findBookById:(). <br/>
     * 
     * @param id
     * @author wanghb
     * @return
     * @since JDK 1.8
     */
    public Book findBookById(Long id);
    
    /** 
     * delBook:(). <br/>
     * 
     * @param id
     * @author wanghb
     * @return
     * @since JDK 1.8
     */
    public void delBook(Long id);
    
    /**updateBook();
     * 
     * 更新图书
     * 
     * @param book
     * @return
     */
    void updateBook(Book book);
    
    /**syncFile();
     * 
     * 批量写入学生借阅书籍信息
     * 
     * @param list
     * @param book
     * @return
     */
    void syncFile(List<SyncUserVO> list,Long bookId);
}


/**
 * Revision history
 * -------------------------------------------------------------------------
 * 
 * Date Author Note
 * -------------------------------------------------------------------------
 * 2017年9月6日 wanghb create
 */