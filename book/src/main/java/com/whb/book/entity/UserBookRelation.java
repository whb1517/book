/*
 * Project: book_manage
 * 
 * File Created at 2017年6月11日
 * 
 * Copyright 2016 CMCC Corporation Limited.
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * ZYHY Company. ("Confidential Information").  You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license.
 */
package com.whb.book.entity;

import java.io.Serializable;
import java.util.Date;

/**
 * @Type userBookRelation.java
 * @Desc 
 * @author wanghb
 * @date 2017年6月11日 下午2:37:22
 * @version 
 */
public class UserBookRelation implements Serializable{

    /**
     * serialVersionUID:TODO().
     * 
     */
    private static final long serialVersionUID = 1L;
    private Long id;
    private Long userId;
    private Long bookId;
    private int borrowStatus;
    private Long createId;
    private Long updateId;
    private Date createTime;
    private Date updateTime;
    public Long getId() {
        return id;
    }
    public void setId(Long id) {
        this.id = id;
    }
    public Long getUserId() {
        return userId;
    }
    public void setUserId(Long userId) {
        this.userId = userId;
    }
    public Long getBookId() {
        return bookId;
    }
    public void setBookId(Long bookId) {
        this.bookId = bookId;
    }
    public int getBorrowStatus() {
        return borrowStatus;
    }
    public void setBorrowStatus(int borrowStatus) {
        this.borrowStatus = borrowStatus;
    }
    public Long getCreateId() {
        return createId;
    }
    public void setCreateId(Long createId) {
        this.createId = createId;
    }
    public Long getUpdateId() {
        return updateId;
    }
    public void setUpdateId(Long updateId) {
        this.updateId = updateId;
    }
    public Date getCreateTime() {
        return createTime;
    }
    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }
    public Date getUpdateTime() {
        return updateTime;
    }
    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }
    @Override
    public String toString() {
        return "UserBookRelation [id=" + id + ", userId=" + userId + ", bookId=" + bookId
                + ", borrowStatus=" + borrowStatus + ", createId=" + createId + ", updateId="
                + updateId + ", createTime=" + createTime + ", updateTime=" + updateTime + "]";
    }
    
}


/**
 * Revision history
 * -------------------------------------------------------------------------
 * 
 * Date Author Note
 * -------------------------------------------------------------------------
 * 2017年6月11日 wanghb create
 */