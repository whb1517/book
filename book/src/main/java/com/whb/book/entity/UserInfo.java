/*
 * Project: book_manage
 * 
 * File Created at 2017年6月11日
 * 
 * Copyright 2016 CMCC Corporation Limited.
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * ZYHY Company. ("Confidential Information").  You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license.
 */
package com.whb.book.entity;

import java.io.Serializable;
import java.util.Date;

/**
 * @Type userInfo.java
 * @Desc 
 * @author wanghb
 * @date 2017年6月11日 下午2:34:37
 * @version 
 */
public class UserInfo implements Serializable{

    /**
     * serialVersionUID:TODO().
     * 
     */
    private static final long serialVersionUID = 1L;
    private Long id;
    private String staffId;
    private String username;
    private int gender;
    private Date birth;
    private String msisdn;
    private String email;
    private String hometown;
    private Date entryTime;
    private String college;
    private String major;
    private int grade;
    private int role;
    private Long createId;
    private Long updateId;
    private Date createTime;
    private Date updateTime;
    public Long getId() {
        return id;
    }
    public void setId(Long id) {
        this.id = id;
    }
    public String getStaffId() {
        return staffId;
    }
    public void setStaffId(String staffId) {
        this.staffId = staffId;
    }
    public String getUsername() {
        return username;
    }
    public void setUsername(String username) {
        this.username = username;
    }
    public int getGender() {
        return gender;
    }
    public void setGender(int gender) {
        this.gender = gender;
    }
    public Date getBirth() {
        return birth;
    }
    public void setBirth(Date birth) {
        this.birth = birth;
    }
    public String getMsisdn() {
        return msisdn;
    }
    public void setMsisdn(String msisdn) {
        this.msisdn = msisdn;
    }
    public String getEmail() {
        return email;
    }
    public void setEmail(String email) {
        this.email = email;
    }
    public String getHometown() {
        return hometown;
    }
    public void setHometown(String hometown) {
        this.hometown = hometown;
    }
    public Date getEntryTime() {
        return entryTime;
    }
    public void setEntryTime(Date entryTime) {
        this.entryTime = entryTime;
    }
    public String getCollege() {
        return college;
    }
    public void setCollege(String college) {
        this.college = college;
    }
    public String getMajor() {
        return major;
    }
    public void setMajor(String major) {
        this.major = major;
    }
    public int getGrade() {
        return grade;
    }
    public void setGrade(int grade) {
        this.grade = grade;
    }
    public int getRole() {
        return role;
    }
    public void setRole(int role) {
        this.role = role;
    }
    public Long getCreateId() {
        return createId;
    }
    public void setCreateId(Long createId) {
        this.createId = createId;
    }
    public Long getUpdateId() {
        return updateId;
    }
    public void setUpdateId(Long updateId) {
        this.updateId = updateId;
    }
    public Date getCreateTime() {
        return createTime;
    }
    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }
    public Date getUpdateTime() {
        return updateTime;
    }
    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }
    @Override
    public String toString() {
        return "UserInfo [id=" + id + ", staffId=" + staffId + ", username=" + username
                + ", gender=" + gender + ", birth=" + birth + ", msisdn=" + msisdn + ", email="
                + email + ", hometown=" + hometown + ", entryTime=" + entryTime + ", college="
                + college + ", major=" + major + ", grade=" + grade + ", role=" + role
                + ", createId=" + createId + ", updateId=" + updateId + ", createTime=" + createTime
                + ", updateTime=" + updateTime + "]";
    }
    
}


/**
 * Revision history
 * -------------------------------------------------------------------------
 * 
 * Date Author Note
 * -------------------------------------------------------------------------
 * 2017年6月11日 wanghb create
 */