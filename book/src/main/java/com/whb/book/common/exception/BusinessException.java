/*
 * Project: book
 * 
 * File Created at 2017年9月15日
 * 
 * Copyright 2016 CMCC Corporation Limited.
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * ZYHY Company. ("Confidential Information").  You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license.
 */
package com.whb.book.common.exception;

/**
 * @Type BusinessException.java
 * @Desc 
 * @author wanghb
 * @date 2017年9月15日 下午7:53:35
 * @version 
 */
public abstract class BusinessException extends RuntimeException implements BusinessError{

    private static final long serialVersionUID = -3201149715174770724L;

    private ErrorCode errorCode;

    /**
     * 
     * @param errorCode
     */
    public BusinessException(ErrorCode errorCode) {
        this.errorCode = errorCode;
    }
    
    /**
     * 
     * @param errorCode
     * @param message
     */
    public BusinessException(ErrorCode errorCode,String message) {
        super(message);
        this.errorCode = errorCode;
    }
    
    /**
     * 
     * @param errorCode
     * @param cause
     */
    public BusinessException(ErrorCode errorCode,Throwable cause) {
        super(cause);
        this.errorCode = errorCode;
    }
    
    /**
     * 
     * @param errorCode
     * @param message
     * @param cause
     */
    public BusinessException(ErrorCode errorCode,String message,Throwable cause) {
        super(message,cause);
        this.errorCode = errorCode;
    }

    @Override
    public ErrorCode getErrorCode() {
        return errorCode;
    }

    @Override
    public synchronized Throwable fillInStackTrace(){
        return this;
    }
    
    
}


/**
 * Revision history
 * -------------------------------------------------------------------------
 * 
 * Date Author Note
 * -------------------------------------------------------------------------
 * 2017年9月15日 wanghb create
 */