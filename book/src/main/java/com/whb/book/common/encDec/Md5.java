/*
 * Project: book
 * 
 * File Created at 2017年9月12日
 * 
 * Copyright 2016 CMCC Corporation Limited.
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * ZYHY Company. ("Confidential Information").  You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license.
 */
package com.whb.book.common.encDec;

import com.google.common.base.Charsets;
import com.google.common.hash.HashFunction;
import com.google.common.hash.Hashing;

/**
 * @Type Md5.java
 * @Desc 
 * @author wanghb
 * @date 2017年9月12日 下午1:11:49
 * @version 
 */
public class Md5 {

    private static HashFunction hf = Hashing.md5();

    /**
     * 
     * getMd5:(); Description:TODO
     * 
     * @author wanghb
     * @param str
     * @return
     * @since JDK 1.8
     */
    public static String getMd5(String str) {
        return hf.newHasher().putString(str, Charsets.UTF_8).hash().toString();
    }
    
    public static void main(String[] args) {
        System.out.println(getMd5("123"));
    }


}


/**
 * Revision history
 * -------------------------------------------------------------------------
 * 
 * Date Author Note
 * -------------------------------------------------------------------------
 * 2017年9月12日 wanghb create
 */