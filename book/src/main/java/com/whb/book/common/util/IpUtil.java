/*
 * Project: book
 * 
 * File Created at 2017年9月11日
 * 
 * Copyright 2016 CMCC Corporation Limited.
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * ZYHY Company. ("Confidential Information").  You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license.
 */
package com.whb.book.common.util;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.regex.Pattern;

import org.apache.commons.lang3.StringUtils;

/**
 * @Type IpUtil.java
 * @Desc 
 * @author wanghb
 * @date 2017年9月11日 下午10:16:32
 * @version 
 */
public class IpUtil {
    
    private static final byte[] LOCAL_IPV4;
    private static final String LOCAL_IPV4_STR;

    static {
        try {
            LOCAL_IPV4 = InetAddress.getLocalHost().getAddress();
        } catch (UnknownHostException e) {
            throw new RuntimeException(e);
        }
        LOCAL_IPV4_STR = StringUtils.join(new int[] { LOCAL_IPV4[0] & 0xff, LOCAL_IPV4[1] & 0xff, LOCAL_IPV4[2] & 0xff, LOCAL_IPV4[3] & 0xff}, '.');
    }
    
    private static final Pattern IPV4_PATTERN = Pattern.compile("^(\\d{1,3}\\.){3}\\d{1,3}$");

    /**
     * 是否IP4地址
     * 
     * @param ip
     * @return
     */
    public static boolean isIpv4(String ip) {
        return IPV4_PATTERN.matcher(ip).matches();
    }
    
    /**
     * @param ip
     * @return
     */
    public static byte[] parseIpv4(int ip) {
        byte[] ipStage = new byte[4];
        ipStage[3] = (byte) (ip & 0xff);
        ip >>>= 8;
        ipStage[2] = (byte) (ip & 0xff);
        ip >>>= 8;
        ipStage[1] = (byte) (ip & 0xff);
        ip >>>= 8;
        ipStage[0] = (byte) (ip & 0xff);
        return ipStage;
    }
    
    /**
     * @param ip
     * @return
     */
    public static byte[] parseIpv4(String ip) {
        String[] ipStage = ip.split("\\.");
        return new byte[] { 
                (byte) Integer.parseInt(ipStage[0]), 
                (byte) Integer.parseInt(ipStage[1]),
                (byte) Integer.parseInt(ipStage[2]), 
                (byte) Integer.parseInt(ipStage[3]) };
    }
    
    /**
     * ipv4 to int
     * 
     * @param ip
     * @return
     */
    public static int ipv42Int(String ip) {
        byte[] ipStage = parseIpv4(ip);
        return ((ipStage[0] & 0xff) << 24) | ((ipStage[1] & 0xff) << 16) | ((ipStage[2] & 0xff) << 8) | (ipStage[3] & 0xff);
    }
    
    /**
     * int to ipv4
     * 
     * @param ip
     * @return
     */
    public static String int2Ipv4(int ip) {
        byte[] ipStage = parseIpv4(ip);
        return StringUtils.join(new int[] { ipStage[0] & 0xff, ipStage[1] & 0xff, ipStage[2] & 0xff, ipStage[3] & 0xff}, '.');
    }

    public static byte[] getLocalIpv4() {
        return LOCAL_IPV4;
    }
    
    public static String getLocalIpv4Str() {
        return LOCAL_IPV4_STR;
    }

}


/**
 * Revision history
 * -------------------------------------------------------------------------
 * 
 * Date Author Note
 * -------------------------------------------------------------------------
 * 2017年9月11日 wanghb create
 */