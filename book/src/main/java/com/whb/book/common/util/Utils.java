/*
 * Project: book
 * 
 * File Created at 2017年9月13日
 * 
 * Copyright 2016 CMCC Corporation Limited.
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * ZYHY Company. ("Confidential Information").  You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license.
 */
package com.whb.book.common.util;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.io.BufferedReader;
import java.io.OutputStream;
import java.security.SecureRandom;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Random;
import java.util.UUID;
import javax.imageio.ImageIO;
import javax.servlet.http.HttpServletRequest;
import org.apache.commons.lang3.StringUtils;
import com.google.common.base.Splitter;

/**
 * @Type Utils.java
 * @Desc 
 * @author wanghb
 * @date 2017年9月13日 下午8:36:34
 * @version 
 */
public class Utils {

    /**
     * 
     * readReqMsg:(). <br/>
     * 
     * 
     * @author wanghb
     * @param request
     * @return
     * @since JDK 1.8
     */
    public static String readReqMsg(HttpServletRequest request) {
        StringBuffer reqMsg = new StringBuffer();
        BufferedReader reader;
        try {
            reader = request.getReader();
            String str = "";
            while ((str = reader.readLine()) != null) {
                reqMsg.append(str);
            }
            return reqMsg.toString();
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    /**
     * 
     * filterStr:(). <br/>
     * 
     * 过滤字符串前后空格以及空赋值
     * 
     * @author wanghb
     * @param str
     * @return
     * @since JDK 1.8
     */
    public static String filterStr(String str) {
        return str = (str == null ? "" : str.trim());
    }

    /**
     * 
     * lenOk:(). <br/>
     * 
     * 
     * @author wanghb
     * @param str
     * @param minLen
     * @param maxLen
     * @return
     * @since JDK 1.8
     */
    public static boolean lenOk(String str, int minLen, int maxLen) {
        if (str.length() >= minLen && str.length() <= maxLen) {
            return true;
        }
        return false;
    }

    /**
     * 
     * splitStr2List:(). <br/>
     * 
     * 
     * @author wanghb
     * @param str
     * @param seperator
     * @return
     * @since JDK 1.8
     */
    public static List<String> splitStr2List(String str, String seperator) {
        Iterator<String> iter = Splitter.on(seperator).trimResults().omitEmptyStrings().split(str)
                .iterator();
        List<String> list = new ArrayList<String>();
        while (iter.hasNext()) {
            list.add(iter.next());
        }
        return list;
    }

    private static Random r = new SecureRandom();//为了安全，因为Random不是真正的随机数
    
    /**
     * 
     * genRandomNum:(). <br/>
     * 
     * 生成指定长度的随机数字
     * 
     * @author wanghb
     * @param len
     * @return
     * @since JDK 1.8
     */
    public static String genRandomNum(int len) {
        StringBuffer sb = new StringBuffer();
        for (int i = 0; i < len; i++) {
            sb.append(r.nextInt(10));
        }
        return sb.toString();
    }

    /**
     * 
     * uuid:(). <br/>
     * 
     * 
     * @author wanghb
     * @param len
     * @return
     * @since JDK 1.8
     */
    public static String uuid() {
        return UUID.randomUUID().toString().replaceAll("-", "");
    }

    /**
     * 
     * genRandomHexString:().<br/>
     * 
     * 获取16进制随机数
     * 
     * @author whb
     * @param len
     * @return
     * @since JDK 1.8
     */
    public static String genRandomHexString(int len) {
        StringBuffer sb = new StringBuffer();
        for (int i = 0; i < len; i++) {
            sb.append(Integer.toHexString(r.nextInt(16)));
        }
        return sb.toString().toUpperCase();

    }

    private static String pwdLetter = "abcdefjhijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";
    private static String pwdNum = "0123456789";

    /**
     * 
     * genPwd:(). <br/>
     * 
     * 生成密码（四位字母+四位数字）
     * 
     * @author wanghb
     * @return
     * @since JDK 1.8
     */
    public static String genPwd() {
        StringBuffer sb = new StringBuffer();
        for (int i = 0; i < 4; i++) {
            sb.append(pwdLetter.charAt(r.nextInt(pwdLetter.length() - 1)));
        }
        for (int i = 0; i < 4; i++) {
            sb.append(pwdNum.charAt(r.nextInt(pwdNum.length() - 1)));
        }
        return sb.toString();
    }

    /**
     * 
     * randomNum:(). <br/>
     * 
     * 
     * @author wanghb
     * @param begin
     * @param end
     * @return
     * @since JDK 1.8
     */
    public static int randomNum(int begin, int end) {
        return r.nextInt(end - begin) + begin;
    }

    private static String base = "ABCDEFGHKMNPRWX2345689defhijkmnpqwxyz";

    /**
     * genCode:(). <br/>
     * 
     * 图形验证码
     * 
     * @param len
     * @return
     */
    public static String genCode(int len) {
        StringBuffer sb = new StringBuffer();
        for (int i = 0; i < len; i++) {
            sb.append(base.charAt(r.nextInt(base.length() - 1)));
        }
        return sb.toString();
    }

    /**
     * 
     * code:(). <br/>
     * 图验样式
     * 
     * @author wanghb
     * @param content
     * @param out
     * @throws Exception
     * @since JDK 1.8
     */
    public static void code(String content, OutputStream out) throws Exception {
        int width = 120;
        int height = 30;
        // 干扰颜色
        Color[] interColor = new Color[] { new Color(176, 196, 222), new Color(245, 222, 179),
                new Color(255, 228, 225), new Color(192, 192, 192) };
        // 字符颜色
        Color[] wordColor = new Color[] { new Color(47, 79, 79), new Color(0, 128, 128),
                new Color(95, 158, 160), new Color(70, 130, 180), new Color(105, 105, 105) };
        BufferedImage image = new BufferedImage(width, height, BufferedImage.TYPE_INT_RGB);
        Graphics g = image.getGraphics();
        // 背景画图
        g.setColor(Color.WHITE);
        g.fillRect(0, 0, width, height);
        // 边框画图
        g.setColor(Color.WHITE);
        g.drawRect(1, 1, width - 2, height - 2);
        // 干扰画图
        for (int i = 0; i < 20; i++) {
            int x1 = r.nextInt(width);
            int y1 = r.nextInt(height);
            int x2 = r.nextInt(width);
            int y2 = r.nextInt(height);
            g.setColor(interColor[r.nextInt(interColor.length - 1)]);
            g.drawLine(x1, y1, x2, y2);
        }
        Graphics2D g2 = (Graphics2D) g;
        Font[] fonts = new Font[] { new Font("宋体", Font.BOLD, 30), new Font("黑体", Font.PLAIN, 30),
                new Font("楷体", Font.ITALIC, 30), };
        int x = 5;
        for (int i = 0; i < content.length(); i++) {
            g.setFont(fonts[r.nextInt(fonts.length - 1)]);
            g.setColor(wordColor[r.nextInt(wordColor.length - 1)]);
            int degree = r.nextInt() % 30;//
            String ch = String.valueOf(content.charAt(i));
            g2.rotate(degree * Math.PI / 180, x, 5);// 设置旋转角度
            g2.drawString(ch, x, randomNum(20, 30));
            g2.rotate(-degree * Math.PI / 180, x, 5);// 旋转回来
            x += randomNum(15, 20);
        }
        ImageIO.write(image, "jpg", out);
        out.flush();
        out.close();
    }

    /**
     * 
     * htmlFilter:(). <br/>
     * 
     * 过滤HTML标签
     * 
     * @author wanghb
     * @param content
     * @return
     * @since JDK 1.8
     */
    public static String htmlFilter(String content) {
        if (StringUtils.isEmpty(content)) {
            return "";
        } else {
            String html = content;
            html = StringUtils.replace(html, "'", "&apos;");
            html = StringUtils.replace(html, "\"", "&quot;");
            html = StringUtils.replace(html, "\t", "&nbsp;&nbsp;");// 替换跳格
            html = StringUtils.replace(html, " ", "&nbsp;");// 替换空格
            html = StringUtils.replace(html, "<", "&lt;");
            html = StringUtils.replace(html, ">", "&gt;");
            return html;
        }
    }

}


/**
 * Revision history
 * -------------------------------------------------------------------------
 * 
 * Date Author Note
 * -------------------------------------------------------------------------
 * 2017年9月13日 wanghb create
 */