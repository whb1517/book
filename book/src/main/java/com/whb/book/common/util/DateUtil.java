/*
 * Project: book
 * 
 * File Created at 2017年9月13日
 * 
 * Copyright 2016 CMCC Corporation Limited.
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * ZYHY Company. ("Confidential Information").  You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license.
 */
package com.whb.book.common.util;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.time.DateUtils;
import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;


/**
 * @Type DateUtil.java
 * @Desc 
 * @author wanghb
 * @date 2017年9月13日 下午8:25:45
 * @version 
 */
public class DateUtil {

    public final static String YYYYMMDDHHMMSS = "yyyyMMddhhmmss";
    
    public final static String YYYYMMDDHHMMSSSSS = "yyyyMMddHHmmssSSS";

    public final static String YYYYMMDDHHMMSSWITHSYMBOL = "yyyy-MM-dd HH:mm:ss";
    
    public final static String YYYYMMDDWITHSYMBOL = "yyyy-MM-dd";

    public final static String YYYYMMDD = "yyyyMMdd";
    
    public final static String YYYYMMDDHHMMSSSSSWITHSYMBOL = "yyyy-MM-dd HH:mm:ss:SSS";
    
    /**
     * <pre>
     * 指定Pattern 格式化时间
     * 
     * date = parseToDate("2000-01-01",FORMAT_DATE_TIME)
     * </pre>
     * 
     * @param strTime
     *            pattern对应的时间
     * @param pattern
     *            strTime的格式
     * @see #FORMAT_DATE
     * @see #FORMAT_TIME
     * @see #FORMAT_DATE_TIME
     * @see #FORMAT_MONTH_DAY_TIME
     * @return 格式化结果
     * @throws UnsupportedOperationException
     *             if parsing is not supported
     * @throws IllegalArgumentException
     *             if the text to parse is invalid
     * 
     */
    public static Date s2d(String strTime, String pattern) {
        if (StringUtils.isEmpty(strTime)) {
            return null;
        }
        DateTime d = DateTimeFormat.forPattern(pattern).parseDateTime(strTime);
        return d.toDate();
    }
    
    /**
     * 
     * s2dHours24:(). <br/>
     * 
     * Stirng 转 Date(24小时制)
     * 
     * @author wanghb
     * @param strTime
     *            pattern对应的时间
     * @param pattern
     *            strTime的格式
     * @return
     * @since JDK 1.8
     */
    public static Date s2dHours24(String strTime, String pattern) {
        if (StringUtils.isEmpty(strTime)) {
            return null;
        }
        Date d = null;
        try {
            d = new SimpleDateFormat(pattern).parse(strTime);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return d;
    }

    /**
     * 
     * @param date
     * @param pattern
     * @return
     */
    public static String d2s(Date date, String pattern) {
        if (date == null) {
            return null;
        }
        DateTime time = new DateTime(date);
        return time.toString(pattern);
    }

    /**
     * 
     * @return
     */
    public static Date getCurrentDate() {
        return new Date();
    }

    /**
     * 
     * getYear:(). <br/>
     * 
     * 
     * @author wanghb
     * @param now
     * @return
     * @since JDK 1.8
     */
    public static String getYear(Date now) {
        return String.valueOf(new DateTime(now).getYear());
    }

    /**
     * 
     * getMonth:(). <br/>
     * 
     * 
     * @author wanghb
     * @param now
     * @return
     * @since JDK 1.8
     */
    public static String getMonth(Date now) {
        return String.valueOf(new DateTime(now).getMonthOfYear());
    }

    /**
     * 
     * getDay:(). <br/>
     * 
     * 
     * @author wanghb
     * @param now
     * @return
     * @since JDK 1.8
     */
    public static String getDay(Date now) {
        return String.valueOf(new DateTime(now).getDayOfMonth());
    }
    
    /**
     * 
     * getYesterday:(). <br/>
     * 
     * 获取昨天日期
     * 
     * @author wanghb
     * @return
     * @since JDK 1.8
     */
    public static Date getYesterday(){
        Date today = getCurrentDate();
        return DateUtils.addDays(today, -1);
    }
    
    /**
     * 
     * getTomorrow:(). <br/>
     * 
     * 获取明天日期
     * 
     * @author wanghb
     * @return
     * @since JDK 1.8
     */
    public static Date getTomorrow(){
        Date today = getCurrentDate();
        return DateUtils.addDays(today, 1);
    }

    public static void main(String[] args) {
        Date now = new Date();
        System.out.println(now);
        System.out.println(getYesterday());
        System.out.println(getTomorrow());
    }

}


/**
 * Revision history
 * -------------------------------------------------------------------------
 * 
 * Date Author Note
 * -------------------------------------------------------------------------
 * 2017年9月13日 wanghb create
 */