/*
 * Project: book
 * 
 * File Created at 2017年9月11日
 * 
 * Copyright 2016 CMCC Corporation Limited.
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * ZYHY Company. ("Confidential Information").  You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license.
 */
package com.whb.book.common;

import java.net.URI;

/**
 * @Type Constants.java
 * @Desc 
 * @author wanghb
 * @date 2017年9月11日 下午9:59:17
 * @version 
 */
public interface Constants {
    
    String GENDER_MALE = "1";// 男
    String GENDER_FEMALE = "0";// 女
    
    String LOGIN_COOKIE_KEY = "user_login_cookie";
    int LOGIN_CACHE_TIME = 3600;// 用户登录缓存1个小时
    
    int NO_SPECIAL_ROLE = -1;
    int ROLE_USER = 0;// 学生登录的角色
    int ROLE_TEACHER = 1;// 老师登录的角色
    
    String BASE_PATH = SystemConfig.get("base.path");
    String DOMAIN = URI.create(BASE_PATH).getHost();
    
    String MOBILE_PATTERN = SystemConfig.get("mobile.pattern");
    String TELECOM_PATTERN = SystemConfig.get("telecom.pattern");
    String UNICOM_PATTERN = SystemConfig.get("unicom.pattern");

}


/**
 * Revision history
 * -------------------------------------------------------------------------
 * 
 * Date Author Note
 * -------------------------------------------------------------------------
 * 2017年9月11日 wanghb create
 */