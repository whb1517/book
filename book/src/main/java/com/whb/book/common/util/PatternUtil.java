/*
 * Project: book
 * 
 * File Created at 2017年9月11日
 * 
 * Copyright 2016 CMCC Corporation Limited.
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * ZYHY Company. ("Confidential Information").  You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license.
 */
package com.whb.book.common.util;

import org.apache.oro.text.regex.MalformedPatternException;
import org.apache.oro.text.regex.Pattern;
import org.apache.oro.text.regex.PatternCompiler;
import org.apache.oro.text.regex.PatternMatcher;
import org.apache.oro.text.regex.PatternMatcherInput;
import org.apache.oro.text.regex.Perl5Compiler;
import org.apache.oro.text.regex.Perl5Matcher;

/**
 * @Type PatternUtil.java
 * @Desc 
 * @author wanghb
 * @date 2017年9月11日 下午10:18:19
 * @version JDK 1.8
 */
public class PatternUtil {

    private static PatternCompiler compiler = new Perl5Compiler();
    private static Pattern emailPattern = null;
    private static Pattern genderPattern = null;
    private static Pattern urlPattern = null;

    static{
        try {
            emailPattern = compiler.compile("^([a-zA-Z0-9\\_\\-\\.])+@([a-zA-Z0-9\\_\\-\\.])+(\\.([a-zA-Z0-9])+)+$");
            genderPattern = compiler.compile("^(0|1)$");
            // http https url
            urlPattern = compiler
                    .compile("^(http|https)://[A-Za-z0-9_.#:?/]{0,200}[A-Za-z0-9_]{0,200}$");
        } catch (MalformedPatternException e) {
            throw new RuntimeException("正则加载失败", e);
        }
    }
    
    /**
     * isEmail:(). <br/>
     * 
     * @author wanghb
     * @param email
     * @return
     * @since JDK 1.8
     */
    public static boolean isEmail(String email) {
        PatternMatcher matcher = new Perl5Matcher();
        if (matcher.matches(new PatternMatcherInput(email), emailPattern)) {
            return true;
        }
        return false;
    }
    
    /**
     * isGender:(). <br/>
     * 
     * @author wanghb
     * @param gender
     * @return
     * @since JDK 1.8
     */
    public static boolean isGender(String gender) {
        PatternMatcher matcher = new Perl5Matcher();
        if (matcher.matches(new PatternMatcherInput(gender), genderPattern)) {
            return true;
        }
        return false;
    }
    
    /**
     * isUrl:(). <br/>
     * 
     * @author wanghb
     * @param url
     * @return
     * @since JDK 1.8
     */
    public static boolean isUrl(String url) {
        PatternMatcher matcher = new Perl5Matcher();
        if (matcher.matches(new PatternMatcherInput(url), urlPattern)) {
            return true;
        }
        return false;
    }
}


/**
 * Revision history
 * -------------------------------------------------------------------------
 * 
 * Date Author Note
 * -------------------------------------------------------------------------
 * 2017年9月11日 wanghb create
 */