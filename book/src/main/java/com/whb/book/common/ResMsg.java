/*
 * Project: book
 * 
 * File Created at 2017年9月8日
 * 
 * Copyright 2016 CMCC Corporation Limited.
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * ZYHY Company. ("Confidential Information").  You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license.
 */
package com.whb.book.common;

/**
 * @Type ResMsg.java
 * @Desc 
 * @author wanghb
 * @date 2017年9月8日 下午4:08:00
 * @version 
 */
public enum ResMsg {

    // 2XX 前端输入约束验证错误
    // 3XX 系统逻辑判断错误
    // 4XX 权限返回错误
    // 5XX 内部系统错误
    
    SUCCESS(100,"成功"),
    
    GENDER_ERROR(201,"性别错误"),
    
    MSISDN_ERROR(202,"手机号错误"),
    
    PARAM_ERROR(301,"参数错误"),
    
    SENSITIVE_ERROR(302,"输入框含有敏感词"),
    
    FILE_ERROR(303,"文件错误"),
    
    SYS_ERROR(500,"系统错误"),
    
    NO_ROLE_ERROR(520, "无权访问"),
    
    LOGIN_TIMEOUT_ERROR(550,"登录超时/未登录"),
    ;
    
    private int code;
    private String msg;
    private ResMsg(int code, String msg) {
        this.code = code;
        this.msg = msg;
    }
    public int getCode() {
        return code;
    }
    public String getMsg() {
        return msg;
    }
    
    
    
    

}


/**
 * Revision history
 * -------------------------------------------------------------------------
 * 
 * Date Author Note
 * -------------------------------------------------------------------------
 * 2017年9月8日 wanghb create
 */