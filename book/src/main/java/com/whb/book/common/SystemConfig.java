/*
 * Project: book
 * 
 * File Created at 2017年9月11日
 * 
 * Copyright 2016 CMCC Corporation Limited.
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * ZYHY Company. ("Confidential Information").  You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license.
 */
package com.whb.book.common;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import org.apache.commons.lang3.StringUtils;

/**
 * @Type SystemConfig.java
 * @Desc 
 * @author wanghb
 * @date 2017年9月11日 下午10:00:37
 * @version 
 */
public class SystemConfig {

    private static Properties prop = new Properties();
    
    static{
        InputStream in = null;
        try {
            in = Thread.currentThread().getContextClassLoader().getResourceAsStream("system.properties");
            prop.load(in);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }finally {
            try {
                in.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
    
    public static String get(String key){
        String str = prop.getProperty(key);
        if(StringUtils.isBlank(str) || StringUtils.isBlank(str.trim())){
            return "";
        }
        return str.trim();
    }
}


/**
 * Revision history
 * -------------------------------------------------------------------------
 * 
 * Date Author Note
 * -------------------------------------------------------------------------
 * 2017年9月11日 wanghb create
 */