/*
 * Project: book
 * 
 * File Created at 2017年9月18日
 * 
 * Copyright 2016 CMCC Corporation Limited.
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * ZYHY Company. ("Confidential Information").  You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license.
 */
package com.whb.book.common.interceptor;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.commons.lang3.StringUtils;

/**
 * @Type CookieManager.java
 * @Desc 
 * @author wanghb
 * @date 2017年9月18日 下午3:05:32
 * @version 
 */
public class CookieManager {

    private HttpServletRequest request;
    private HttpServletResponse response;
    
    public CookieManager(HttpServletRequest request, HttpServletResponse response) {
        this.request = request;
        this.response = response;
    }
    
    public String getRemoteIp(){
        String ip = request.getHeader("x-real-ip");
        if (StringUtils.isNotBlank(ip)) {
            return ip;
        }
        ip = request.getHeader("x-forwarded-for");
        if (StringUtils.isBlank(ip)) {
            return request.getRemoteAddr();
        }
        return ip.split(",\\s*")[0];
    }
    
    /**
     * 设置cookie
     * @param name
     * @param value
     * @param domain
     * @param expire
     */
    public void setCookie(String name,String value,String domain,int expire){
        Cookie cookie = new Cookie(name, value);
        cookie.setDomain(domain);
        cookie.setPath("/");
        if(expire >= 0){
            cookie.setMaxAge(expire);
        }
        //HttpOnly标识是一个可选的、避免利用XSS（Cross-Site Scripting)来获取session cookie的标识。
        //XSS攻击最常见一个的目标是通过获取的session cookie来劫持受害者的session；使用HttpOnly标识是一种很有用的保护机制。
        cookie.setHttpOnly(true);
        response.addCookie(cookie);
    }
    
    /**
     * 获取某个cookie值
     * @param name
     * @return
     */
    public String getCookieValue(String name){
        Cookie[] cookies = request.getCookies();
        if(cookies == null){
            return null;
        }
        Cookie cookie = null;
        for(int i =0;i<cookies.length;i++){
            cookie = cookies[i];
            if(name.equalsIgnoreCase(cookie.getName())){
                return cookie.getValue();
            }
        }
        return null;
    }
    
    /**
     * setCookie
     * @param name
     * @param value
     * @param domain
     */
    public void setCookie(String name,String value,String domain){
        //-1表示关闭浏览器则cookie失效
        setCookie(name, value, domain, -1);
    }
    
    /**
     * 清除某一个cookie
     * @param name
     * @param domain
     */
    public void clearCookie(String name,String domain){
        setCookie(name,"", domain, 0);
    }
    
    /**
     * 清除某一个session
     * @param name
     */
    public void clearSession(String name){
        request.getSession().removeAttribute(name);
    }
}


/**
 * Revision history
 * -------------------------------------------------------------------------
 * 
 * Date Author Note
 * -------------------------------------------------------------------------
 * 2017年9月18日 wanghb create
 */