/*
 * Project: book
 * 
 * File Created at 2017年9月18日
 * 
 * Copyright 2016 CMCC Corporation Limited.
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * ZYHY Company. ("Confidential Information").  You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license.
 */
package com.whb.book.common.interceptor;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import com.whb.book.common.Constants;
import com.whb.book.common.encDec.Md5;
import com.whb.book.common.redis.RedisUtil;

/**
 * @Type SessionCache.java
 * @Desc 
 * @author wanghb
 * @date 2017年9月18日 下午4:51:03
 * @version 
 */
public class SessionCache {

    private static Logger logger = LoggerFactory.getLogger(SessionCache.class);
    
    @Autowired
    private RedisUtil redisUtil;
    
    public UserSession getLoginCache(CookieManager cm) {
        String cookieValue = cm.getCookieValue(Constants.LOGIN_COOKIE_KEY);
        if (StringUtils.isNotEmpty(cookieValue)) {
            try {
                UserSession us = (UserSession) redisUtil.get(cookieValue);
                if (us != null) {
                    redisUtil.expire(cookieValue, Constants.LOGIN_CACHE_TIME);
                }
                return us;
            } catch (Exception e) {
                throw new RuntimeException("获取登录缓存有误");
            }
        }
        return null;
    }
    
    /**
     * 缓存登录用户的信息
     * 
     * @param userName
     * @param cm
     */
    public void cacheLoginInfo(UserSession us, CookieManager cm) {
        String clientIp = cm.getRemoteIp();
        logger.info("当前登录用户IP {}", clientIp);
        String sessionKey = Md5.getMd5(us.getMsisdn() + clientIp);
        // 根据手机号查询用户信息，初始化UserSession
        redisUtil.set(sessionKey, us, Constants.LOGIN_CACHE_TIME);
        cm.setCookie(Constants.LOGIN_COOKIE_KEY, sessionKey, Constants.DOMAIN);
        logger.info("设置登录缓存，设置cookie");
    }
    
    /**
     * 清除缓存中登录用户的信息
     * @param cm
     */
    public void deleteLoginCache(CookieManager cm) {
        String cookieValue = cm.getCookieValue(Constants.LOGIN_COOKIE_KEY);
        if (StringUtils.isNotBlank(cookieValue)) {
            try {
                redisUtil.remove(cookieValue);
                cm.clearSession(cookieValue);
            } catch (Exception e) {
                throw new RuntimeException("清除登录缓存有误");
            }
            cm.clearCookie(Constants.LOGIN_COOKIE_KEY, Constants.DOMAIN);
            logger.info("清除浏览器cookie，清除登录缓存");
        }
    }
}


/**
 * Revision history
 * -------------------------------------------------------------------------
 * 
 * Date Author Note
 * -------------------------------------------------------------------------
 * 2017年9月18日 wanghb create
 */