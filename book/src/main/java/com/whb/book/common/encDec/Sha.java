/*
 * Project: book
 * 
 * File Created at 2017年9月12日
 * 
 * Copyright 2016 CMCC Corporation Limited.
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * ZYHY Company. ("Confidential Information").  You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license.
 */
package com.whb.book.common.encDec;

import com.google.common.base.Charsets;
import com.google.common.hash.HashFunction;
import com.google.common.hash.Hashing;

/**
 * @Type Sha.java
 * @Desc 
 * @author wanghb
 * @date 2017年9月12日 下午1:23:35
 * @version 
 */
public class Sha {

    //Hashing.sha1() == Sha1Holder.SHA_1 == new MessageDigestHashFunction("SHA-1", "Hashing.sha1()");
    private static HashFunction sha1 = Hashing.sha1();
    //new MessageDigestHashFunction("SHA-256", "Hashing.sha256()");
    private static HashFunction sha256 = Hashing.sha256();
    //new MessageDigestHashFunction("SHA-512", "Hashing.sha512()");
    private static HashFunction sha512 = Hashing.sha512();
    
    /**
     * getSha1:(). <br/>
     * 
     * @param str
     * @return
     * @since JDK 1.8
     */
    public static String getSha1(String str){
        return sha1.newHasher().putString(str, Charsets.UTF_8).hash().toString();
    }
    
    /**
     * getSha256:(). <br/>
     * 
     * @param str
     * @return
     * @since JDK 1.8
     */
    public static String getSha256(String str){
        return sha256.newHasher().putString(str, Charsets.UTF_8).hash().toString();
    }
    
    /**
     * getSha512:(). <br/>
     * 
     * @param str
     * @return
     * @since JDK 1.8
     */
    public static String getSha512(String str){
        return sha512.newHasher().putString(str, Charsets.UTF_8).hash().toString();
    }
    
    public static void main(String[] args) {
        //40bd001563085fc35165329ea1ff5c5ecbdbbeef--40λ
        System.out.println(Sha.getSha1("123"));
        
        //a665a45920422f9d417e4867efdc4fb8a04a1f3fff1fa07e998e86f7f7a27ae3--64λ
        System.out.println(Sha.getSha256("123"));
        
        //128λ
        //3c9909afec25354d551dae21590bb26e38d53f2173b8d3dc3eee4c047e7ab1c1eb8b85103e3be7ba613b31bb5c9c36214dc9f14a42fd7a2fdb84856bca5c44c2
        System.out.println(Sha.getSha512("123"));
    }

}


/**
 * Revision history
 * -------------------------------------------------------------------------
 * 
 * Date Author Note
 * -------------------------------------------------------------------------
 * 2017年9月12日 wanghb create
 */