/*
 * Project: book_manage
 * 
 * File Created at 2017年9月6日
 * 
 * Copyright 2016 CMCC Corporation Limited.
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * ZYHY Company. ("Confidential Information").  You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license.
 */
package com.whb.book.common;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

/**
 * @Type ActionResponse.java
 * @Desc 
 * @author wanghb
 * @date 2017年9月6日 下午10:49:58
 * @version 
 */
public class ActionResponse implements Serializable{

    private static final long serialVersionUID = 1L;
    
    private static final String RESULT_KEY = "result";

    private static final String REDIRECT_KEY = "redirect";

    private static final String DATA_KEY = "data";

    private static final String PAGE_KEY = "page";

    public static final int SUCCESS_RESULT = 2000;

    public static final int FAIL_RESULT = 1000;

    private static final Map<Integer, String> MESSAGES = new HashMap<Integer, String>();

    private int status = SUCCESS_RESULT;

    private String message = MESSAGES.get(SUCCESS_RESULT);

    private Map<String, Object> results = new HashMap<String, Object>();

    public void setStatus(int status) {
        this.status = status;
        this.message = MESSAGES.get(status);
    }

    public void setStatusAndMessage(int status, String message) {
        this.status = status;
        this.message = message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public void setResult(Object value) {
        results.put(RESULT_KEY, value);
    }

    public void setRedirectInResult(Object value) {
        results.put(REDIRECT_KEY, value);
    }

    public void setDataInResult(Object value) {
        results.put(DATA_KEY, value);
    }

    public void setPageInResult(Object value) {
        results.put(PAGE_KEY, value);
    }

    /**
     * 
     */
    public void reset() {
        setStatus(SUCCESS_RESULT);
        results.clear();
    }

    /**
     * 
     * @param key
     * @param value
     */
    public void put(String key, Object value) {
        results.put(key, value);
    }

    /**
     * 
     * @param values
     */
    public void putAll(Map<String, Object> values) {
        results.putAll(values);
    }

    public int getStatus() {
        return status;
    }

    public String getMessage() {
        return message;
    }

    public Object getResult() {
        if (results.size() == 1) {
            Object result = results.get(RESULT_KEY);
            if (result != null) {
                return result;
            }
        }
        return results;
    }

}


/**
 * Revision history
 * -------------------------------------------------------------------------
 * 
 * Date Author Note
 * -------------------------------------------------------------------------
 * 2017年9月6日 wanghb create
 */