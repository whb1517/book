/*
 * Project: book
 * 
 * File Created at 2017年9月11日
 * 
 * Copyright 2016 CMCC Corporation Limited.
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * ZYHY Company. ("Confidential Information").  You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license.
 */
package com.whb.book.common.util;

import org.apache.oro.text.regex.MalformedPatternException;
import org.apache.oro.text.regex.Pattern;
import org.apache.oro.text.regex.PatternCompiler;
import org.apache.oro.text.regex.PatternMatcher;
import org.apache.oro.text.regex.PatternMatcherInput;
import org.apache.oro.text.regex.Perl5Compiler;
import org.apache.oro.text.regex.Perl5Matcher;

import com.whb.book.common.Constants;


/**
 * @Type MsisdnUtil.java
 * @Desc 
 * @author wanghb
 * @date 2017年9月11日 下午8:27:24
 * @version 
 */
public class MsisdnUtil {

    private static PatternCompiler compiler = new Perl5Compiler();
    private static Pattern mobileMsisdnPattern = null;
    private static Pattern unicomMsisdnPattern = null;
    private static Pattern teleComMsisdnPattern = null;

    static {
        try {
            // 中国移动
            mobileMsisdnPattern = compiler.compile(Constants.MOBILE_PATTERN);
            // 中国联通
            unicomMsisdnPattern = compiler.compile(Constants.UNICOM_PATTERN);
            // 中国电信
            teleComMsisdnPattern = compiler.compile(Constants.TELECOM_PATTERN);
        } catch (MalformedPatternException e) {
            throw new RuntimeException("正则加载失败", e);
        }
    }

    /**
     * 
     * isMobileMsiSdn:(). <br/>
     * 
     * 
     * @author whb
     * @param msisdn
     * @return
     * @since JDK 1.8
     */
    public static boolean isMobileMsiSdn(String msisdn) {
        PatternMatcher matcher = new Perl5Matcher();
        if (matcher.matches(new PatternMatcherInput(msisdn), mobileMsisdnPattern)) {
            return true;
        }
        return false;
    }

    /**
     * 
     * isUnicomMsisdn:(). <br/>
     * 
     * 
     * @author whb
     * @param msisdn
     * @return
     * @since JDK 1.8
     */
    public static boolean isUnicomMsisdn(String msisdn) {
        PatternMatcher matcher = new Perl5Matcher();
        if (matcher.matches(new PatternMatcherInput(msisdn), unicomMsisdnPattern)) {
            return true;
        }
        return false;
    }

    /**
     * 
     * isTelecomMsisdn:(). <br/>
     * 
     * 
     * @author whb
     * @param msisdn
     * @return
     * @since JDK 1.8
     */
    public static boolean isTelecomMsisdn(String msisdn) {
        PatternMatcher matcher = new Perl5Matcher();
        if (matcher.matches(new PatternMatcherInput(msisdn), teleComMsisdnPattern)) {
            return true;
        }
        return false;
    }

    /**
     * 
     * isMsiSdn:(). <br/>
     * 
     * 
     * @author whb
     * @param msisdn
     * @return
     * @since JDK 1.8
     */
    public static boolean isMsisdn(String msisdn) {
        if (isMobileMsiSdn(msisdn) || isUnicomMsisdn(msisdn) || isTelecomMsisdn(msisdn)) {
            return true;
        }
        return false;
    }
}


/**
 * Revision history
 * -------------------------------------------------------------------------
 * 
 * Date Author Note
 * -------------------------------------------------------------------------
 * 2017年9月11日 wanghb create
 */