/*
 * Project: book
 * 
 * File Created at 2017年9月15日
 * 
 * Copyright 2016 CMCC Corporation Limited.
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * ZYHY Company. ("Confidential Information").  You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license.
 */
package com.whb.book.common.exception;

/**
 * @Type BookException.java
 * @Desc 
 * @author wanghb
 * @date 2017年9月15日 下午8:05:15
 * @version 
 */
public class BookException extends BusinessException{

    private static final long serialVersionUID = -6448330075328441414L;

    public BookException(ErrorCode errorCode) {
        super(errorCode);
    }
    
    public BookException(ErrorCode errorCode,String message) {
        super(errorCode,message);
    }

}


/**
 * Revision history
 * -------------------------------------------------------------------------
 * 
 * Date Author Note
 * -------------------------------------------------------------------------
 * 2017年9月15日 wanghb create
 */