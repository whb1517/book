/*
 * Project: book
 * 
 * File Created at 2017年9月15日
 * 
 * Copyright 2016 CMCC Corporation Limited.
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * ZYHY Company. ("Confidential Information").  You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license.
 */
package com.whb.book.dao;

import java.util.List;

import com.whb.book.entity.UserInfo;

/**
 * @Type UserDao.java
 * @Desc 
 * @author wanghb
 * @date 2017年9月15日 下午7:32:49
 * @version 
 */
public interface UserDao {

    /**queryById();
     * 
     * 通过id去查询学生
     * 
     * @param id
     * @return
     */
    UserInfo query(Long id);
    
    /**queryByMsisdn();
     * 
     * 通过手机号去查询学生
     * 
     * @param id
     * @return
     */
    UserInfo queryByMsisdn(String msisdn);

    /**delete();
     * 
     * 通过id去删除学生
     * 
     * @param id
     * @return
     */
    void update(Long id);

    /**update();
     * 
     * 更新学生信息
     * 
     * @param book
     * @return
     */
    void update(UserInfo user);

    /**
    * queryAll();
    * 
    * 查询所有学生
    * 
    * @return
    */
    List<UserInfo> queryAll();
    
    /**
     * save();
     * 
     * 保存学生信息
     * 
     * @return
     */
    void save(UserInfo user);
}

/**
 * Revision history
 * -------------------------------------------------------------------------
 * 
 * Date Author Note
 * -------------------------------------------------------------------------
 * 2017年9月15日 wanghb create
 */
