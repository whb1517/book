/*
 * Project: book
 * 
 * File Created at 2017年9月15日
 * 
 * Copyright 2016 CMCC Corporation Limited.
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * ZYHY Company. ("Confidential Information").  You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license.
 */
package com.whb.book.dao;


import com.whb.book.entity.UserBookRelation;

/**
 * @Type UserDao.java
 * @Desc 
 * @author wanghb
 * @date 2017年9月15日 下午7:32:49
 * @version 
 */
public interface UserBookRelationDao {

    
    UserBookRelation query(Long userId,Long bookId);
    
    void save(UserBookRelation ubr);
    
    void update(UserBookRelation ubr);
    
}

/**
 * Revision history
 * -------------------------------------------------------------------------
 * 
 * Date Author Note
 * -------------------------------------------------------------------------
 * 2017年9月15日 wanghb create
 */
