/*
 * Project: book_manage
 * 
 * File Created at 2017年6月11日
 * 
 * Copyright 2016 CMCC Corporation Limited.
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * ZYHY Company. ("Confidential Information").  You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license.
 */
package com.whb.book.dao;

import java.util.List;

import com.whb.book.entity.Book;

/**
 * @Type BookDao.java
 * @Desc 
 * @author wanghb
 * @date 2017年6月11日 下午2:40:00
 * @version 
 */
public interface BookDao {

    /**query();
     * 
     * 通过id去查询单本图书
     * 
     * @param id
     * @return
     */
    Book query(Long id);

    /**delete();
     * 
     * 通过id去删除单本图书
     * 
     * @param id
     * @return
     */
    void delete(Long id);

    /**update();
     * 
     * 更新图书
     * 
     * @param book
     * @return
     */
    void update(Book book);

    /**
     * queryAll();
     * 
     * 查询所有图书
     * 
     * @return
     */
    List<Book> queryAll();

    /**
     * 
     * reduceNumber();
     * 
     * 减少馆藏数量
     * 
     * @param id
     * @return 如果影响行数等于>1，表示更新的记录行数
     */
    int reduceNumber(Long id);

}

/**
 * Revision history
 * -------------------------------------------------------------------------
 * 
 * Date Author Note
 * -------------------------------------------------------------------------
 * 2017年6月11日 wanghb create
 */
